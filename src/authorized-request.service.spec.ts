import {
	Injector
} from '@angular/core';
import {
	TestBed,
	getTestBed
} from '@angular/core/testing';
import { map } from 'rxjs/operators';

import {
	Sha1Service
} from './sha1.service';
import {
	OAuthService
} from './oauth.service';
import {
	AuthorizedRequestService
} from './authorized-request.service';

import { appkey, token } from './apikeys';
import { HttpClientModule, HttpClient } from '@angular/common/http';

describe('AuthorizedRequestService',()=>{
	let injecter: Injector;
	let request : AuthorizedRequestService;
	beforeEach(()=>{
		TestBed.configureTestingModule({
			imports: [HttpClientModule]
		});
		injecter = getTestBed();

		request = new AuthorizedRequestService(
			new OAuthService(new Sha1Service),
			injecter.get(HttpClient)
		);
	});

	it('can get 5 tweets from home_timeline.',(done)=>{
		request.get(
			'https://api.twitter.com/1.1/statuses/home_timeline.json',
			{
				count:5
			},
			appkey,
			token
		).pipe(map((res: any) => res.json()))
		.subscribe(
			(res: any[])=>{
				expect(res.length).toBe(5);
				done();
			},(err: Response)=>{
				expect(err.status).toBe(200);
				done();
			}
		);
	});

	it('can get 20 tweets from home_timeline.',(done)=>{
		request.get(
			'https://api.twitter.com/1.1/statuses/home_timeline.json',
			{
			},
			appkey,
			token
		).pipe(
			map((res: Response) => res.json())
		).subscribe(
			(res: any) => {
				expect(res.length).toBe(20);
				done();
			}, (err: Response) => {
				expect(err.status).toBe(200);
				done();
			});
		});

	it('can update status.',(done)=>{
		request.post(
			'https://api.twitter.com/1.1/statuses/update.json',
			{
				status: "hoge hoge"
			},
			appkey,
			token
		).pipe(map((res: any) => res.json()))
		.subscribe(
			(res)=>{
				expect(res.text).toBe("hoge hoge");
				done();
			},(err)=>{
				expect(err.status).toBe(200);
				done();
			}
		);
	});
});